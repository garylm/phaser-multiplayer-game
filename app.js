var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);
var bodyparser = require("body-parser");		
var db = require('./db.js');
var Player = require('./Player');


app.use('/css',express.static(__dirname + '/css'));
app.use('/js',express.static(__dirname + '/js'));
app.use('/assets',express.static(__dirname + '/assets'));

app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json({ type: "application/json" }));

app.get('/',function(req,res){
    res.sendFile(__dirname+'/index.html');
});


var socket;  // Socket controller
var players = []; // Array of connected players


io.on('connection',function(client){

   client.on('disconnect', onClientDisconnect);

  // Listen for new player message
  client.on('new player', onNewPlayer);

  // Listen for move player message
  client.on('move player', onMovePlayer);

  client.on('killStar', onKillStar);
  
  client.on('sendScore', onSendScore);

  client.on('getScore', onGetScore);

  client.on('test',function(){
        console.log('test received');
    });

});


server.listen(process.env.PORT || 8081,function(){
    console.log('Listening on '+server.address().port);
});


function onClientDisconnect () {
 // util.log('Player has disconnected: ' + this.id)

  var removePlayer = playerById(this.id);

  // Player not found
  if (!removePlayer) {
    console.log('Player not found: ' + this.id);
   // util.log('Player not found: ' + this.id)
    return;
  }

  // Remove player from players array
  players.splice(players.indexOf(removePlayer), 1);

  // Broadcast removed player to connected socket clients
  this.broadcast.emit('remove player', {id: this.id});
}



// New player has joined
function onNewPlayer (data) {
  // Create a new player
  var newPlayer = new Player(data.x, data.y);
  newPlayer.id = this.id;
  newPlayer.name = this.name;

  // Broadcast new player to connected socket clients
  this.broadcast.emit('new player', {id: newPlayer.id,name:newPlayer.name, x: newPlayer.getX(), y: newPlayer.getY()});

  // Send existing players to the new player
  var i, existingPlayer;
  for (i = 0; i < players.length; i++) {
    existingPlayer = players[i];
    this.emit('new player', {id: existingPlayer.id,name:newPlayer.name, x: existingPlayer.getX(), y: existingPlayer.getY()});
  }

  // Add new player to the players array
  players.push(newPlayer);
}



    function onMovePlayer (data) {
    // Find player in array
        var movePlayer = playerById(this.id);

    // Player not found
    if (!movePlayer) {
      console.log('PlayerMove not found:' + this.id);
    //util.log('Player not found: ' + this.id)
    return;
  }

  // Update player position
  movePlayer.setX(data.x);
  movePlayer.setY(data.y);
  //movePlayer.setAngle(data.angle)

  // Broadcast updated position to connected socket clients
  this.broadcast.emit('move player', {id: movePlayer.id, x: movePlayer.getX(), y: movePlayer.getY()});
    }



function getAllPlayers(){
    var players = [];
    Object.keys(io.sockets.connected).forEach(function(socketID){
        var player = io.sockets.connected[socketID].player;
        if(player) players.push(player);
    });
    return players;
}


function randomInt (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}



/* ************************************************
** GAME HELPER FUNCTIONS
************************************************ */
// Find player by ID
function playerById (id) {
  var i;
  for (i = 0; i < players.length; i++) {
    if (players[i].id === id) {
      return players[i];
    }
  }

  return false;
}


function onKillStar(data){
  //console.log(data.id);
  this.broadcast.emit('killStar', {id: data.id });
}


function onSendScore(data){
  var record = {name:this.id,score:data};
  //console.log(data.id);
  db.insertScore(record);  
  console.log(data);
}

function onGetScore(){
db.getScore(function(score){
		this.emit('getScore',score);
});
}

//leaderboard functions

app.get('/getScore', function(req, res)	{
db.getScore(function(score){
		res.send(score);
});
});


app.post('/putScore', function(req, res)	{
db.insertScore(res.body);
res.send(200);
});













