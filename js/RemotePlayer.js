var RemotePlayer = function (index,name, game, player, startX, startY) {
  var x = startX;
  var y = startY;
  //var angle = startAngle

  this.game = game;
  //this.health = 3
  this.player = player;
  this.alive = true;
  
  var playerText;


    this.player = game.add.sprite(32, game.world.height - 150, 'dude');

    //  We need to enable physics on the player
    game.physics.arcade.enable(this.player);

    //  Player physics properties. Give the little guy a slight bounce.
    this.player.body.bounce.y = 0.2;
    this.player.body.gravity.y = 300;
    this.player.body.collideWorldBounds = true;

    //  Our two animations, walking left and right.
    this.player.animations.add('left', [0, 1, 2, 3], 10, true);
    this.player.animations.add('right', [5, 6, 7, 8], 10, true);

  //this.player = game.add.sprite(x, y, 'dude')

//  this.player.animations.add('left', [0, 1, 2, 3, 4, 5, 6, 7], 20, true)
 // this.player.animations.add('rif', [3], 20, true)

  //this.player.anchor.setTo(0.5, 0.5)

  this.player.id = index.toString();
  this.player.name = name;


    this.playerText = game.add.text(this.player.x, this.player.y - 20, this.player.name, {fontSize: '25px', fill: '#000' });

  //game.physics.enable(this.player, Phaser.Physics.ARCADE)
  //this.player.body.immovable = true
  //this.player.body.collideWorldBounds = true

  //this.player.angle = angle
    cursors = game.input.keyboard.createCursorKeys();
  this.lastPosition = { x: x, y: y };


}

RemotePlayer.prototype.update = function () {
  
  this.player.body.velocity.x = 0;

  if (this.player.x !== this.lastPosition.x || this.player.y !== this.lastPosition.y) {
        if (cursors.left.isDown)
    {
        //  Move to the left
        this.player.body.velocity.x = -150;

        this.player.animations.play('left');
    }
    else if (cursors.right.isDown)
    {
        //  Move to the right
        this.player.body.velocity.x = 150;

        this.player.animations.play('right');
    }
    else
    {
        //  Stand still
        this.player.animations.stop();

        this.player.frame = 4;
    }
    
    //  Allow the player to jump if they are touching the ground.
    if (cursors.up.isDown && this.player.body.touching.down)
    {
        this.player.body.velocity.y = -350;
    }
}
//    this.player.play('move');
    //this.player.rotation = Math.PI + game.physics.arcade.angleToXY(this.player, this.lastPosition.x, this.lastPosition.y)
 // } else {
   // this.player.play('stop');
  //}

  this.playerText.x = this.player.x;
  this.playerText.y = this.player.y-20;


  this.lastPosition.x = this.player.x;
  this.lastPosition.y = this.player.y;
  //this.lastPosition.angle = this.player.angle
}

window.RemotePlayer = RemotePlayer;