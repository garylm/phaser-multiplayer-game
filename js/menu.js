

var Menu = function(){
    console.log("MenuState");
};

Menu.prototype = {
    preload : preloadMain,
    create : createMain,
    update : updateMain
};


function preloadMain(){
    game.load.image('start', 'assets/start.png');
    game.load.image('score', 'assets/score.png');
    game.load.image('help', 'assets/help.png');
    game.load.image('sky', 'assets/sky.png');
    game.load.image('mountain', 'assets/mountains.jpg');

}

var startButton;
var helpButton;
var scoreButton;

function createMain(){
        var mountain = game.add.sprite(0, 0, 'mountain');
        
        mountain.scale.setTo(1.5,1.5);
        startButton = game.add.button(game.world.centerX - 95, 200, 'start', startGame, this);

        helpButton = game.add.button(game.world.centerX - 95, 300, 'help', showHelp, this);
        
        scoreButton = game.add.button(game.world.centerX - 95, 400, 'score', startLeaderBoard, this);

}

function updateMain(){

}



var startGame = function(){
game.state.start('Game');
}

var startLeaderBoard = function(){
game.state.start('LeaderBoard');
}

var showHelp = function(){
game.state.start('Help');
}