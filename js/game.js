
var Game = {};

Game.preload = function() {

    game.load.image('sky', 'assets/sky.png');
    game.load.image('ground', 'assets/platform.png');
    game.load.image('star', 'assets/star.png');
    game.load.spritesheet('dude', 'assets/dude.png', 32, 48);

}

var player;
var platforms;
var cursors;

var stars;
var score = 0;
var scoreText;

var Client = {};

var enemies;

var playerText;

var nickName;


var setEventHandlers = function(){
  Client.socket.on('connect', onSocketConnected);

  // Socket disconnection
  Client.socket.on('disconnect', onSocketDisconnect);

  // New player message received
  Client.socket.on('new player', onNewPlayer);

  // Player move message received
  Client.socket.on('move player', onMovePlayer);

  // Player removed message received
  Client.socket.on('remove player', onRemovePlayer);

  Client.socket.on('killStar', onKillStar);
}
  

Client.sendTest = function(){
    console.log("test sent");
    Client.socket.emit('test');
};

Client.sendScore = function(score){
    console.log("score sent");
    Client.socket.emit('sendScore',score);
};







function onSocketConnected () {
  console.log('Connected to socket server');


  // Reset enemies on reconnect
  enemies.forEach(function (enemy) {
    enemy.player.kill();
  });
  enemies = [];

  // Send local player data to the game server
  Client.socket.emit('new player', { x: player.x, y: player.y,name:nickName });
}

// Socket disconnected
function onSocketDisconnect () {
  console.log('Disconnected from socket server');
}



Game.create = function() {
    Client.socket = io.connect();
    
    nickName = prompt("What is your nickname?")

    console.log(nickName);
    //  We're going to be using physics, so enable the Arcade Physics system
    game.physics.startSystem(Phaser.Physics.ARCADE);

    //  A simple background for our game
    game.add.sprite(0, 0, 'sky');

    //  The platforms group contains the ground and the 2 ledges we can jump on
    platforms = game.add.group();

    //  We will enable physics for any object that is created in this group
    platforms.enableBody = true;

    // Here we create the ground.
    var ground = platforms.create(0, game.world.height - 64, 'ground');

    //  Scale it to fit the width of the game (the original sprite is 400x32 in size)
    ground.scale.setTo(2, 2);

    //  This stops it from falling away when you jump on it
    ground.body.immovable = true;

    //  Now let's create two ledges
    var ledge = platforms.create(400, 400, 'ground');
    ledge.body.immovable = true;

    ledge = platforms.create(-150, 250, 'ground');
    ledge.body.immovable = true;

    // The player and its settings
    player = game.add.sprite(32, game.world.height - 150, 'dude');

    //  We need to enable physics on the player
    game.physics.arcade.enable(player);

    //  Player physics properties. Give the little guy a slight bounce.
    player.body.bounce.y = 0.2;
    player.body.gravity.y = 300;
    player.body.collideWorldBounds = true;

    //  Our two animations, walking left and right.
    player.animations.add('left', [0, 1, 2, 3], 10, true);
    player.animations.add('right', [5, 6, 7, 8], 10, true);

    enemies = [];
    //  Finally some stars to collect
    stars = game.add.group();

    //  We will enable physics for any star that is created in this group
    stars.enableBody = true;

    //  Here we'll create 12 of them evenly spaced apart
    for (var i = 0; i < 12; i++)
    {
        //  Create a star inside of the 'stars' group
        var star = stars.create(i * 70, 0, 'star');

        //  Let gravity do its thing
        star.body.gravity.y = 300;

        //  This just gives each star a slightly random bounce value
        star.body.bounce.y = 0.7 + Math.random() * 0.2;
        star.id=i;
    }

    //  The score
    scoreText = game.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#000' });

    //  Our controls.
    cursors = game.input.keyboard.createCursorKeys();
  
    playerText = game.add.text(player.x, player.y - 20, nickName,{ fontSize: '25px', fill: '#000' });
    setEventHandlers();
}


Game.update  = function() {

    for (var i = 0; i < enemies.length; i++) {
    if (enemies[i].alive) {
      enemies[i].update();
      game.physics.arcade.collide(player, enemies[i].player);
      game.physics.arcade.collide(enemies[i].player, platforms);
    }
  }

  playerText.x = player.x;
  playerText.y = player.y-20;

    //  Collide the player and the stars with the platforms
    game.physics.arcade.collide(player, platforms);
    game.physics.arcade.collide(stars, platforms);

    //  Checks to see if the player overlaps with any of the stars, if he does call the collectStar function
    game.physics.arcade.overlap(player, stars, collectStar, null, this);

    //  Reset the players velocity (movement)
    player.body.velocity.x = 0;

    if (cursors.left.isDown)
    {
        //  Move to the left
        player.body.velocity.x = -150;

        player.animations.play('left');
    }
    else if (cursors.right.isDown)
    {
        //  Move to the right
        player.body.velocity.x = 150;

        player.animations.play('right');
    }
    else
    {
        //  Stand still
        player.animations.stop();

        player.frame = 4;
    }
    
    //  Allow the player to jump if they are touching the ground.
    if (cursors.up.isDown && player.body.touching.down)
    {
        player.body.velocity.y = -350;
    }
 
  Client.socket.emit('move player', { x: player.x, y: player.y});

}

function collectStar (player, star) {
    
    // Removes the star from the screen
    star.kill();
    Client.socket.emit('killStar',{id:star.id});
    stars.remove(star);
    //  Add and update the score
    score += 10;
    scoreText.text = 'Score: ' + score;
//    console.log(stars.length);
    if (stars.length==0) {
      Client.sendScore(score);
        //game.state.stop('gGam');
        game.state.start('LeaderBoard');
    }

}



// New player
function onNewPlayer (data) {
  console.log('New player connected:', data.id);

  // Avoid possible duplicate players
  var duplicate = playerById(data.id);
  if (duplicate) {
    console.log('Duplicate player!');
    return;
  }

  // Add new player to the remote players array
  enemies.push(new RemotePlayer(data.id,data.name, game, player, data.x, data.y));
}

// Move player
function onMovePlayer (data) {
  var movePlayer = playerById(data.id);
  //console.log("MovePlsa:"+playerById(data.id));
  // Player not found
  if (!movePlayer) {
    console.log('Player not found: ', data.id);
    return;
  }

  // Update player position
  movePlayer.player.x = data.x;
  movePlayer.player.y = data.y;
  //movePlayer.player.angle = data.angle;
}

// Remove player
function onRemovePlayer (data) {
  var removePlayer = playerById(data.id);

  // Player not found
  if (!removePlayer) {
    console.log('Player not found: ', data.id);
    return;
  }

  removePlayer.player.kill();

  // Remove player from array
  enemies.splice(enemies.indexOf(removePlayer), 1);
}

function onKillStar(data){
  stars.forEach(function (star) {
    if(star.id===data.id)
    {
      star.kill();
      stars.remove(star);
      //console.log(star.id);
  }
  });
}
// Find player by ID
function playerById (id) {
  for (var i = 0; i < enemies.length; i++) {
  //   console.log(i+":"+enemies[i].player.id);
    if (enemies[i].player.id === id) {
      return enemies[i];
    }
  }

  return false;
}
