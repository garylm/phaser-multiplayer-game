
var Help = function(){
    console.log("helpState");
};

Help.prototype = {
    preload : preloadhelp,
    create : createhelp,
    update : updatehelp
};


function preloadhelp(){
    game.load.image('background', 'assets/help-background.png');

}


function createhelp(){
    var background = game.add.sprite(0, 0, 'background');

   background.scale.setTo(2,2);
    
    helpText = game.add.text(game.world.width/2,game.world.height/4, 'Help', { fontSize: '45px', fill: '#FFF' });
    
    helpText.anchor.setTo(0.5,0.5);

    line1Text = game.add.text(game.world.centerX-200, game.world.height/3, 'This is a multiplayer game.', { fontSize: '32px', fill: '#FFF' });

    line2Text = game.add.text(game.world.centerX-200,game.world.height/2, 'Collect as many coins as you can.', { fontSize: '32px', fill: '#FFF' });
    
    line3Text = game.add.text(game.world.centerX-200,game.world.height/2+100, 'The player who collects', { fontSize: '32px', fill: '#FFF' });
	
	line4Text = game.add.text(game.world.centerX-200,game.world.height/2+200, 'the maximum coins wins.', { fontSize: '32px', fill: '#FFF' }); 
  
}
function updatehelp(){

}

